package com.bignerdranch.android.tpmobile.dal;

/**
 * Created by mlevasseur on 2018-10-14.
 */

public class CriterePerformance {

    private int id;
    private int no;
    private String critere;

    private int idElementCompetence;
    private ElementCompetence elementCompetence;

    public CriterePerformance() {}

    public CriterePerformance(int id, int no, String critere, int idElementCompetence) {
        this.id = id;
        this.no = no;
        this.critere = critere;
        this.idElementCompetence = idElementCompetence;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public String getCritere() {
        return critere;
    }

    public void setCritere(String critere) {
        this.critere = critere;
    }

    public int getIdElementCompetence() {
        return idElementCompetence;
    }

    public void setIdElementCompetence(int idElementCompetence) {
        this.idElementCompetence = idElementCompetence;
    }

    public ElementCompetence getElementCompetence() {
        return elementCompetence;
    }

    public void setElementCompetence(ElementCompetence elementCompetence) {
        this.elementCompetence = elementCompetence;
    }

    @Override
    public boolean equals(Object o) {
        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                CriterePerformance c = (CriterePerformance) o;
                egale = (id == c.id);
            }
        }
        return egale;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
