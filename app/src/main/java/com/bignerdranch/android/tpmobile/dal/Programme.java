package com.bignerdranch.android.tpmobile.dal;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlevasseur on 2018-10-14.
 */

public class Programme {

    private int id;
    private String nom;
    private int duree;

    private List<Competence> competences = new ArrayList<>();
    private List<ChampCompetence> champsCompetences;

    public Programme() {}

    public Programme(int id) {
        this.id = id;
    }

    public Programme(int id, String nom, int duree) {
        this.id = id;
        this.nom = nom;
        this.duree = duree;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public List<Competence> getCompetences() {
        return competences;
    }

    public List<ChampCompetence> getChampsCompetences() {
        return champsCompetences;
    }

    @Override
    public boolean equals(Object o) {

        boolean egale = false;

        if (o != null) {
            if (getClass() == o.getClass()) {

                Programme c = (Programme) o;
                egale = (id == c.id);
            }
        }
        return egale;
    }

    @Override
    public int hashCode() {
        int result = id;
        return result;
    }
}
