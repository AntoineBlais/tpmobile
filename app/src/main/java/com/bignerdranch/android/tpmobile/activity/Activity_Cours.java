package com.bignerdranch.android.tpmobile.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.bignerdranch.android.tpmobile.Fragment.Fragment_Cours;
import com.bignerdranch.android.tpmobile.R;

public class Activity_Cours extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cour);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_cours);
        if (fragment == null) {
            fragment = new Fragment_Cours();
            fm.beginTransaction()
                    .add(R.id.fragment_cours, fragment)
                    .commit();
        }
    }
}
