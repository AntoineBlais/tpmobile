package com.bignerdranch.android.tpmobile.adapter;

import android.widget.ArrayAdapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.android.tpmobile.R;
import com.bignerdranch.android.tpmobile.dal.Cours;

import java.util.ArrayList;
import java.util.List;

public class CoursAdapter extends ArrayAdapter {
    private Context mContext;
    private List<Cours> coursList = new ArrayList<>();

    public CoursAdapter(Context context, List<Cours> list) {
        super(context, 0 , list);
        mContext = context;
        coursList = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem = convertView;
        Cours c = coursList.get(position);

        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.listview_cours_list, parent, false);

        TextView name = (TextView) listItem.findViewById(R.id.cArrayName);
        name.setText(c.getSigleCour());

        TextView desc = (TextView) listItem.findViewById(R.id.cArrayDesc);
        desc.setText(c.getTitreCour());

        return listItem;
    }
}
