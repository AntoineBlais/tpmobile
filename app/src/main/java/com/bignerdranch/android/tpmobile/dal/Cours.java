package com.bignerdranch.android.tpmobile.dal;

import com.google.gson.annotations.SerializedName;

public class Cours {
    @SerializedName("Id")
    private int idCour;

    @SerializedName("Sigle")
    private String sigleCour;

    @SerializedName("Titre")
    private String titreCour;

    @SerializedName("Description")
    private String descriptionCour;

    @SerializedName("Duree")
    private int dureeCour;

    @SerializedName("Contenu")
    private String contenuCour;

    @SerializedName("Ponderation")
    private String ponderationCour;

    @SerializedName("EpreuveFinale")
    private String epreuveFinaleCour;

    @SerializedName("IdChampCompetences")
    private int idChampCompetences;

    public int getIdCour(){ return idCour; }
    public void setIdCour(int id) { this.idCour = id; }

    public String getSigleCour() { return sigleCour;}
    public void setSigleCour(String sigle) { this.sigleCour = sigle; }

    public String getTitreCour() { return titreCour; }
    public void setTitreCour(String titre) { this.titreCour = titre; }

    public String getDescriptionCour() { return descriptionCour; }
    public void setDescriptionCour(String description) { this.descriptionCour = description; }

    public int getDureeCour() { return dureeCour; }
    public void setDureeCour(int duree) { this.dureeCour = duree; }

    public String getContenuCour() {
        return contenuCour;
    }
    public void setContenuCour(String contenu) {
        this.contenuCour = contenu;
    }

    public String getPonderationCour() {
        return ponderationCour;
    }
    public void setPonderationCour(String ponderation) { this.ponderationCour = ponderation; }

    public String getEpreuveFinaleCour() {
        return epreuveFinaleCour;
    }
    public void setEpreuveFinaleCour(String epreuveFinale) { this.epreuveFinaleCour = epreuveFinale; }

    public int getIdChamoCompetence() {
        return idChampCompetences;
    }
    public void setIdChamoCompetence(int idChampCompetence) { this.idChampCompetences = idChampCompetence; }
}