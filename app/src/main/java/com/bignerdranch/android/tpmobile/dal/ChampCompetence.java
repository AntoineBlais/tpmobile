package com.bignerdranch.android.tpmobile.dal;

import com.google.gson.annotations.SerializedName;

public class ChampCompetence {

    @SerializedName("Id")
    private int idChampComp;

    @SerializedName("Enonce")
    private String enonceChampComp;

    public int getId() { return idChampComp; }
    public void setId(int id) { this.idChampComp = id; }

    public String getEnonce() { return enonceChampComp; }
    public void setEnonce(String enonce) { this.enonceChampComp = enonce; }

    @Override
    public String toString() { return this.getEnonce(); }
}