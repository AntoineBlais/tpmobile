package com.bignerdranch.android.tpmobile.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.*;
import android.widget.*;
import android.content.Intent;
import android.support.annotation.NonNull;
import java.util.List;

import com.bignerdranch.android.tpmobile.R;
import com.bignerdranch.android.tpmobile.activity.*;
import com.bignerdranch.android.tpmobile.api.*;
import com.bignerdranch.android.tpmobile.dal.*;

import retrofit2.*;
import retrofit2.converter.gson.GsonConverterFactory;


public class Fragment_Cours extends Fragment {

    private View view;
    private int idCours;
    private List<ChampCompetence> listChampComp;
    private Cours cours;
    private EditText txtSigle;
    private EditText txtTitre;
    private EditText txtDescription;
    private EditText txtDuree;
    private EditText txtContenu;
    private EditText txtPonderation;
    private EditText txtEpFin;
    private Spinner spinChampComp;
    private Button btnSauvegarder;
    private Button btnAnnuler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        idCours = getActivity().getIntent().getIntExtra("idCour", -1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_cours, container, false);
        getCours();
        return view;
    }

    private void getCours() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api Api = retrofit.create(Api.class);

        Call<Cours> appel = Api.GetCoursParId(idCours);

        appel.enqueue(new Callback<Cours>() {
            @Override
            public void onResponse(@NonNull Call<Cours> appel, @NonNull Response<Cours> reponse) {
                cours = reponse.body();
                getChampCompetence();
            }

            @Override
            public void onFailure(Call<Cours> appel, Throwable throwable) { }
        });
    }

    private void getChampCompetence() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api Api = retrofit.create(Api.class);
        Call<List<ChampCompetence>> appel = Api.listeChampsCompetences();

        appel.enqueue(new Callback<List<ChampCompetence>>() {
            @Override
            public void onResponse(Call<List<ChampCompetence>> appel, Response<List<ChampCompetence>> reponse) {
                listChampComp = reponse.body();
                affichageFormulaire();
            }

            @Override
            public void onFailure(Call<List<ChampCompetence>> appel, Throwable throwable) { }
        });
    }

    public void affichageFormulaire() {
        txtSigle = view.findViewById(R.id.txtSigle);
        txtTitre = view.findViewById(R.id.txtTitre);
        txtDescription = view.findViewById(R.id.txtDescription);
        txtDuree = view.findViewById(R.id.txtDuree);
        txtContenu = view.findViewById(R.id.txtContenu);
        txtPonderation = view.findViewById(R.id.txtPonderation);
        txtEpFin = view.findViewById(R.id.txtEpFin);

        txtSigle.setText(cours.getSigleCour());
        txtTitre.setText(cours.getTitreCour());
        txtDescription.setText(cours.getDescriptionCour());
        txtDuree.setText(String.valueOf(cours.getDureeCour()));
        txtContenu.setText(cours.getContenuCour());
        txtPonderation.setText(cours.getPonderationCour());
        txtEpFin.setText(cours.getEpreuveFinaleCour());

        spinChampComp = view.findViewById(R.id.spinnerChampComp);
        ArrayAdapter<ChampCompetence> adapterChampComp = new ArrayAdapter<>(getActivity().getApplicationContext(), android.R.layout.simple_spinner_item,listChampComp);
        spinChampComp.setAdapter(adapterChampComp);
        for(ChampCompetence champCompetence : listChampComp) {
            spinChampComp.setSelection(listChampComp.indexOf(champCompetence));
        }
        adapterChampComp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        btnSauvegarder = view.findViewById(R.id.btnSauvegarder);
        btnSauvegarder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putCours();
            }
        });
        btnAnnuler = view.findViewById(R.id.btnAnnuler);
        btnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void putCours() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        Api Api = retrofit.create(Api.class);

        Cours c = new Cours();
        c.setIdCour(idCours);
        c.setSigleCour(txtSigle.getText().toString());
        c.setTitreCour(txtTitre.getText().toString());
        c.setDescriptionCour(txtDescription.getText().toString());
        c.setDureeCour(Integer.parseInt(txtDuree.getText().toString()));
        c.setContenuCour(txtContenu.getText().toString());
        c.setPonderationCour(txtPonderation.getText().toString());
        c.setEpreuveFinaleCour(txtEpFin.getText().toString());
        ChampCompetence champC = (ChampCompetence) spinChampComp.getSelectedItem();
        c.setIdChamoCompetence(champC.getId());

        Call<Cours> appel = Api.putCours(idCours, c);
        appel.enqueue(new Callback<Cours>() {
            @Override
            public void onResponse(@NonNull Call<Cours> appel, @NonNull Response<Cours> reponse) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<Cours> appel, Throwable throwable) { }
        });
    }
}
