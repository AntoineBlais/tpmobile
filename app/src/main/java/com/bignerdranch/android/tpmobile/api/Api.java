package com.bignerdranch.android.tpmobile.api;

import com.bignerdranch.android.tpmobile.dal.*;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.*;

public interface Api {
    String  BASE_URL = "http://gestionprogrammemobile.sv55.cmaisonneuve.qc.ca/api/";

    @GET("Cours")
    Call<List<Cours>> listeCours();

    @GET("coursParId/{id}")
    Call<Cours> GetCoursParId(@Path("id") int id);

    @GET("ChampsCompetences")
    Call<List<ChampCompetence>> listeChampsCompetences();

    @GET("ChampsCompetences/{id}")
    Call<ChampCompetence> getChampCompetenceParId(@Path("id") int id);

    @PUT("Cours/{id}")
    Call<Cours>  putCours(@Path("id") int id, @Body Cours cours);
}
