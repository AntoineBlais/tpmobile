package com.bignerdranch.android.tpmobile.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.bignerdranch.android.tpmobile.R;
import com.bignerdranch.android.tpmobile.adapter.CoursAdapter;
import com.bignerdranch.android.tpmobile.api.Api;
import com.bignerdranch.android.tpmobile.dal.Cours;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.bignerdranch.android.tpmobile.api.Api.BASE_URL;

public class MainActivity extends Activity{

    private ListView LV;
    private List<Cours> listeCours;
    protected Retrofit retrofit;
    protected Api service;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(Api.class);

        creerLV();

    }

    private void creerLV() {
        Call<List<Cours>> appelCompetences = service.listeCours();
        appelCompetences.enqueue(new Callback<List<Cours>>() {

            public void onResponse(Call<List<Cours>> call, Response<List<Cours>> response) {

                LV = (ListView) findViewById(R.id.lv);
                listeCours = response.body();
                ArrayAdapter AA = new CoursAdapter(MainActivity.this, listeCours);
                LV.setAdapter(AA);

                    LV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent intent = new Intent(MainActivity.this, Activity_Cours.class);
                            int idCour = listeCours.get(position).getIdCour();
                            intent.putExtra("idCour", idCour);
                            startActivity(intent);
                        }
                    });
            }

            public void onFailure(Call<List<Cours>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
